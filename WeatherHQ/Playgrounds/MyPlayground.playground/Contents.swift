import Foundation

let formatter = DateFormatter()
formatter.dateStyle = .none
formatter.timeStyle = .short
let date = Date(timeIntervalSince1970: 15)
formatter.string(for: date)

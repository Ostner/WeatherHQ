import RxSwift
import CoreLocation

extension Reactive where Base: CLGeocoder {

    func reverseGeocode(_ location: CLLocation) -> Observable<[CLPlacemark]> {
        return Observable.create { observer in
            self.base.reverseGeocodeLocation(location) { placemarks, error in
                if let error = error {
                    observer.onError(error)
                    return
                }
                guard let placemarks = placemarks else {
                    observer.onError("no placemarks")
                    return
                }
                observer.onNext(placemarks)
                observer.onCompleted()
            }
            return Disposables.create()
        }
    }
}

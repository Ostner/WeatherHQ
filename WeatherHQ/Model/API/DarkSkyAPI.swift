import Foundation
import Moya

enum DarkSkyAPI {

    enum Unit: String {
        case auto, ca, uk2, us, si
    }

    enum Language: String {
        case en, de
    }

    case forecast(
           key: String,
           lat: String,
           lon: String,
           units: Unit,
           lang: Language
         )
}

extension DarkSkyAPI: TargetType {
    var baseURL: URL { return URL(string: "https://api.darksky.net")! }

    var path: String {
        switch self {
        case let .forecast(key, lat, lon, _, _):
            return "/forecast/\(key)/\(lat),\(lon)"
        }
    }

    var method: Moya.Method {
        return .get
    }

    var task: Task {
        switch self {
        case let .forecast(_, _, _, units, lang):
            let params = ["units": "\(units)", "lang": "\(lang)"]
            return .requestParameters(
              parameters: params,
              encoding: URLEncoding.queryString
            )
        }
    }

    var sampleData: Data {
        return Data()
    }

    var headers: [String: String]? {
        return nil
    }
}

import Foundation

struct ForecastResponse: Decodable {

    enum CodingKeys: String, CodingKey {
        case currently
        case hourly
        case daily
        // case alerts
    }

    enum Icon: String, Decodable {
        case clearDay = "clear-day"
        case clearNight = "clear-night"
        case rain
        case snow
        case sleet
        case wind
        case fog
        case cloudy
        case partlyCloudyDay = "partly-cloudy-day"
        case partlyCloudyNight = "partly-cloudy-night"
    }

    struct Forecast: Decodable {
        enum CodingKeys: String, CodingKey {
            case time
            case icon
            case temperature
            case rainPropability = "precipProbability"
        }
        let time: TimeInterval
        let icon: Icon
        let temperature: Double
        let rainPropability: Double
    }

    struct Currently: Decodable {
        enum CodingKeys: String, CodingKey {
            case time
            case temperature
            case summary
            case rainPropability = "precipProbability"
            case humidity
            case windSpeed
            case pressure
            case uvIndex
            case ozone
        }
        let time: TimeInterval
        let temperature: Double
        let summary: String
        let rainPropability: Double
        let humidity: Double
        let windSpeed: Double
        let pressure: Double
        let uvIndex: Int
        let ozone: Double
    }

    struct Hourly: Decodable {
        enum CodingKeys: String, CodingKey {
            case summary
            case icon
            case data
        }

        let summary: String
        let icon: Icon
        let data: [Forecast]
    }

    struct Daily: Decodable {

        struct DailyData: Decodable {

            enum CodingKeys: String, CodingKey {
                case sunriseTime
                case sunsetTime
            }

            let sunriseTime: TimeInterval
            let sunsetTime: TimeInterval
        }

        enum CodingKeys: String, CodingKey {
            case summary
            case data
        }

        let summary: String
        let data: [DailyData]
    }

    let currently: Currently
    let hourly: Hourly
    let daily: Daily
}

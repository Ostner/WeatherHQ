import Foundation
import CoreLocation
import RxSwift

protocol LocationServicing {
    var locations: Observable<[CLLocation]> { get }
    var placemarks: Observable<[CLPlacemark]> { get }
}

final class LocationService: LocationServicing {

    private let locationProvider: CLLocationManager
    private let geocoder = CLGeocoder()
    private let bag = DisposeBag()

    init(locationProvider: CLLocationManager) {
        self.locationProvider = locationProvider
    }

    var locations: Observable<[CLLocation]> {
        locationProvider.requestWhenInUseAuthorization()
        locationProvider.startUpdatingLocation()
        return locationProvider.rx.didUpdateLocations
    }

    var placemarks: Observable<[CLPlacemark]> {
        return locations
          .flatMap { locations in
              self.geocoder.rx.reverseGeocode(locations.last!)
          }
    }

}

import Foundation
import RxSwift
import Keys
import Moya
import CoreLocation

protocol ForecastServicing {
    func currentWeather(_ location: CLLocation) -> Observable<CurrentWeather>
    func hourlyWeather(_ location: CLLocation) -> Observable<HourlyForecasts>
}

final class ForecastService: ForecastServicing {

    private let provider: MoyaProvider<DarkSkyAPI>

    var apiKey = WeatherHQKeys().darkSkyApiKey

    var units: DarkSkyAPI.Unit = .si

    var language: DarkSkyAPI.Language = .en

    init(provider: MoyaProvider<DarkSkyAPI>) {
        self.provider = provider
    }

    func forecast(_ location: CLLocation) -> Observable<ForecastResponse> {
        let target = DarkSkyAPI.forecast(
          key: apiKey,
          lat: "\(location.coordinate.latitude)",
          lon: "\(location.coordinate.longitude)",
          units: units,
          lang: language
        )
        return provider.rx.request(target)
          .filterSuccessfulStatusCodes()
          .map(ForecastResponse.self)
          .asObservable()
    }

    func currentWeather(_ location: CLLocation) -> Observable<CurrentWeather> {
        return forecast(location)
          .map(CurrentWeather.makeWithUnits(units))
    }

    func hourlyWeather(_ location: CLLocation) -> Observable<HourlyForecasts> {
        return forecast(location)
          .map(HourlyForecasts.makeWithUnits(units))
    }

}

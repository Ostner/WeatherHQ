import Foundation

struct CurrentWeather {

    let time: TimeInterval
    let temperature: Measurement<UnitTemperature>
    let summary: String
    let rainPropability: Double
    let humidity: Double
    let windSpeed: Measurement<UnitSpeed>
    let pressure: Measurement<UnitPressure>
    let uvIndex: Int
    let sunriseTime: TimeInterval
    let sunsetTime: TimeInterval

}

extension CurrentWeather {

    static func temperatureUnit(with responseUnit: DarkSkyAPI.Unit) -> UnitTemperature {
        switch responseUnit {
        case .si: return .celsius
        default: fatalError("not implemented yet")
        }
    }

    static func speedUnit(with responseUnit: DarkSkyAPI.Unit) -> UnitSpeed {
        switch responseUnit {
        case .si: return .kilometersPerHour
        default: fatalError("not implemented yet")
        }
    }

    static func pressureUnit(with responseUnit: DarkSkyAPI.Unit) -> UnitPressure {
        switch responseUnit {
        case .si: return .hectopascals
        default: fatalError("not implemented yet")
        }
    }

    static func makeWithUnits(
      _ unit: DarkSkyAPI.Unit
    ) -> (ForecastResponse) -> CurrentWeather {
        return { response in
            let time = response.currently.time
            let temperature = Measurement<UnitTemperature>(
              value: response.currently.temperature,
              unit: self.temperatureUnit(with: unit)
            )
            let summary = response.currently.summary
            let rainPropability = response.currently.rainPropability
            let humidity = response.currently.humidity
            let windSpeed = Measurement<UnitSpeed>(
              value: response.currently.windSpeed,
              unit: self.speedUnit(with: unit)
            )
            let pressure = Measurement<UnitPressure>(
              value: response.currently.pressure,
              unit: self.pressureUnit(with: unit)
            )
            let uvIndex = response.currently.uvIndex
            let sunriseTime = response.daily.data.first!.sunriseTime
            let sunsetTime = response.daily.data.first!.sunsetTime
            return CurrentWeather(
              time: time,
              temperature: temperature,
              summary: summary,
              rainPropability: rainPropability,
              humidity: humidity,
              windSpeed: windSpeed,
              pressure: pressure,
              uvIndex: uvIndex,
              sunriseTime: sunriseTime,
              sunsetTime: sunsetTime
            )
        }
    }
}

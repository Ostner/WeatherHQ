import Foundation

enum Icon {
    case clearDay
    case clearNight
    case rain
    case snow
    case sleet
    case wind
    case fog
    case cloudy
    case partlyCloudyDay
    case partlyCloudyNight
}

struct HourlyForecasts {

    struct Forecast {
        let date: Date
        let icon: Icon
        let temperature: Measurement<UnitTemperature>
        let rainPropability: Double
    }

    let summary: String
    let hours: [Forecast]
}

extension HourlyForecasts {

    static func temperatureUnit(with responseUnit: DarkSkyAPI.Unit) -> UnitTemperature {
        switch responseUnit {
        case .si: return .celsius
        default: fatalError("not implemented yet")
        }
    }

    static func icon(with responseIcon: ForecastResponse.Icon) -> Icon {
        switch responseIcon {
        case .clearDay: return .clearDay
        case .clearNight: return .clearNight
        case .rain: return .rain
        case .snow: return .snow
        case .sleet: return .sleet
        case .wind: return .wind
        case .fog: return .fog
        case .cloudy: return .cloudy
        case .partlyCloudyDay: return .partlyCloudyDay
        case .partlyCloudyNight: return .partlyCloudyNight
        }
    }

    static func makeWithUnits(_ units: DarkSkyAPI.Unit) -> (ForecastResponse) -> HourlyForecasts {
        return { response in

            let hours = response.hourly.data.map { response -> HourlyForecasts.Forecast in
                let date = Date(timeIntervalSince1970: response.time)
                let icon = self.icon(with: response.icon)
                let temperature = Measurement<UnitTemperature>(
                  value: response.temperature,
                  unit: self.temperatureUnit(with: units)
                )
                let rainPropability = response.rainPropability
                return HourlyForecasts.Forecast(
                  date: date,
                  icon: icon,
                  temperature: temperature,
                  rainPropability: rainPropability
                )
            }

            return HourlyForecasts(
              summary: response.hourly.summary,
              hours: hours
            )
        }
    }
}

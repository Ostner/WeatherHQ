import UIKit
import RxSwift
import RxCocoa

final class CurrentWeatherViewController:
  UIViewController,
  StoryboardIntitializable {

    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var sublocality: UILabel!
    @IBOutlet weak var temperature: UILabel!
    @IBOutlet weak var summary: UILabel!
    @IBOutlet weak var sunsetIcon: UILabel!
    @IBOutlet weak var sunsetTime: UILabel!
    @IBOutlet weak var rainPropabilityIcon: UILabel!
    @IBOutlet weak var rainPropability: UILabel!
    @IBOutlet weak var humidityIcon: UILabel!
    @IBOutlet weak var humidity: UILabel!
    @IBOutlet weak var windIcon: UILabel!
    @IBOutlet weak var wind: UILabel!
    @IBOutlet weak var pressureIcon: UILabel!
    @IBOutlet weak var pressure: UILabel!
    @IBOutlet weak var uvIndexIcon: UILabel!
    @IBOutlet weak var uvIndex: UILabel!

    var viewModel: CurrentWeatherViewModel!

    private let bag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()
        sunsetIcon.text = "\u{f052}"
        rainPropabilityIcon.text = "\u{f084}"
        humidityIcon.text = "\u{f07a}"
        windIcon.text = "\u{f050}"
        pressureIcon.text = "\u{f079}"
        uvIndexIcon.text = "\u{f072}"
        bindViewModel()
    }

    private func bindViewModel() {
        let trigger = Driver.just(())
        let input = CurrentWeatherViewModel.Input(trigger: trigger)

        let output = viewModel.transform(input)

        output.city
          .drive(city.rx.text)
          .disposed(by: bag)

        output.sublocality
          .drive(sublocality.rx.text)
          .disposed(by: bag)

        output.temperature
          .map(formatTemperature)
          .drive(temperature.rx.text)
          .disposed(by: bag)

        output.summary
          .drive(summary.rx.text)
          .disposed(by: bag)

        output.sunsetTime
          .map(formatTime)
          .drive(sunsetTime.rx.text)
          .disposed(by: bag)

        output.rainPropability
          .map(formatProbablity)
          .drive(rainPropability.rx.text)
          .disposed(by: bag)

        output.humidity
          .map(formatProbablity)
          .drive(humidity.rx.text)
          .disposed(by: bag)

        output.windSpeed
          .map(formatSpeed)
          .drive(wind.rx.text)
          .disposed(by: bag)

        output.pressure
          .map(formatPressure)
          .drive(pressure.rx.text)
          .disposed(by: bag)

        output.uvIndex
          .map(formatIndices)
          .drive(uvIndex.rx.text)
          .disposed(by: bag)
    }

    private func formatTemperature(_ temperature: Measurement<UnitTemperature>?) -> String? {
        guard let temperature = temperature else { return nil }
        let formatter = MeasurementFormatter()
        formatter.unitOptions = .temperatureWithoutUnit
        let numFormatter = NumberFormatter()
        numFormatter.numberStyle = .decimal
        numFormatter.maximumFractionDigits = 0
        formatter.numberFormatter = numFormatter
        return formatter.string(from: temperature)
    }

    private func formatTime(_ time: TimeInterval?) -> String? {
        guard let time = time else { return nil }
        let formatter = DateFormatter()
        formatter.dateStyle = .none
        formatter.timeStyle = .short
        let date = Date(timeIntervalSince1970: time)
        return formatter.string(for: date)
    }

    private func formatProbablity(_ probability: Double?) -> String? {
        guard let probability = probability else { return nil }
        let formatter = NumberFormatter()
        formatter.numberStyle = .percent
        formatter.maximumFractionDigits = 0
        return formatter.string(for: probability)
    }

    private func formatSpeed(_ speed: Measurement<UnitSpeed>?) -> String? {
        guard let speed = speed else { return nil }
        let formatter = MeasurementFormatter()
        let numFormatter = NumberFormatter()
        numFormatter.numberStyle = .decimal
        numFormatter.maximumFractionDigits = 1
        numFormatter.minimumFractionDigits = 1
        formatter.numberFormatter = numFormatter
        return formatter.string(from: speed)
    }

    private func formatPressure(_ pressure: Measurement<UnitPressure>?) -> String? {
        guard let pressure = pressure else { return nil }
        let formatter = MeasurementFormatter()
        formatter.unitOptions = .providedUnit
        let numFormatter = NumberFormatter()
        numFormatter.numberStyle = .decimal
        numFormatter.maximumFractionDigits = 0
        formatter.numberFormatter = numFormatter
        return formatter.string(for: pressure)
    }

    private func formatIndices(_ index: Int?) -> String? {
        guard let index = index else { return nil }
        return String(index)
    }

}

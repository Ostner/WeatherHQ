import UIKit
import RxSwift
import RxCocoa
import Keys
import Moya

final class CurrentWeatherViewModel {

    private let forecastService: ForecastServicing
    private let locationService: LocationServicing

    private let apiKey = WeatherHQKeys().darkSkyApiKey

    init(
      forecastService: ForecastService,
      locationService: LocationServicing
    ) {
        self.forecastService = forecastService
        self.locationService = locationService
    }

    struct Input {
        let trigger: Driver<Void>
    }

    struct Output {
        let city: Driver<String?>
        let sublocality: Driver<String?>
        let temperature: Driver<Measurement<UnitTemperature>?>
        let summary: Driver<String?>
        let sunriseTime: Driver<TimeInterval?>
        let sunsetTime: Driver<TimeInterval?>
        let rainPropability: Driver<Double?>
        let humidity: Driver<Double?>
        let windSpeed: Driver<Measurement<UnitSpeed>?>
        let pressure: Driver<Measurement<UnitPressure>?>
        let uvIndex: Driver<Int?>
    }

    func transform(_ input: Input) -> Output {
        let location = locationService.locations
          .map { $0.last! }
          .share(replay: 1)

        let forecast = input.trigger
            .asObservable()
            .flatMap { _ in location }
            .flatMapLatest { self.forecastService.currentWeather($0) }
            .share(replay: 1)

        let placemark = input.trigger
          .asObservable()
          .flatMapLatest { self.locationService.placemarks }
          .share(replay: 1)

        let city = placemark
          .map { $0.last!.locality }
          .asDriver(onErrorJustReturn: nil)

        let sublocality = placemark
          .map { $0.last!.subLocality }
          .asDriver(onErrorJustReturn: nil)

        let temperature = forecast
          .map { $0.temperature }
          .asDriver(onErrorJustReturn: nil)

        let summary = forecast
          .map { $0.summary }
          .asDriver(onErrorJustReturn: nil)

        let sunriseTime = forecast
          .map { $0.sunriseTime }
          .asDriver(onErrorJustReturn: nil)

        let sunsetTime = forecast
          .map { $0.sunsetTime }
          .asDriver(onErrorJustReturn: nil)

        let rainPropability = forecast
          .map { $0.rainPropability }
          .asDriver(onErrorJustReturn: nil)

        let humidity = forecast
          .map { $0.humidity }
          .asDriver(onErrorJustReturn: nil)

        let windSpeed = forecast
          .map { $0.windSpeed }
          .asDriver(onErrorJustReturn: nil)

        let pressure = forecast
          .map { $0.pressure }
          .asDriver(onErrorJustReturn: nil)

        let uvIndex = forecast
          .map { $0.uvIndex }
          .asDriver(onErrorJustReturn: nil)

        return Output(
          city: city,
          sublocality: sublocality,
          temperature: temperature,
          summary: summary,
          sunriseTime: sunriseTime,
          sunsetTime: sunsetTime,
          rainPropability: rainPropability,
          humidity: humidity,
          windSpeed: windSpeed,
          pressure: pressure,
          uvIndex: uvIndex
        )
    }
}

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

struct HourlyWeatherSection {
    var header: String
    var items: [HourlyForecasts.Forecast]
}

extension HourlyWeatherSection: SectionModelType {
    init(original: HourlyWeatherSection, items: [HourlyForecasts.Forecast]) {
        self = original
        self.items = items
    }
}

final class HourlyWeatherViewController:
  UIViewController,
  StoryboardIntitializable {

    @IBOutlet weak var collectionView: UICollectionView!

    var viewModel: HourlyWeatherViewModel!

    private var dataSource: RxCollectionViewSectionedReloadDataSource<HourlyWeatherSection>!

    private let bag = DisposeBag()

    override func viewDidLoad() {
        super.viewDidLoad()

        dataSource = RxCollectionViewSectionedReloadDataSource<HourlyWeatherSection>(
          configureCell: { dataSource, collectionView, indexPath, item in
              let cell = collectionView.dequeueReusableCell(
                withReuseIdentifier: "Item",
                for: indexPath
              ) as! HourlyWeatherCell
              cell.hour.text = self.formatDate(item.date)
              cell.icon.text = self.iconString(with: item.icon)
              cell.temperature.text = self.formatTemperature(item.temperature)
              cell.temperatureIcon.text = "\u{f055}"
              cell.rainPropability.text = self.formatProbablity(item.rainPropability)
              cell.rainPropabilityIcon.text = "\u{f084}"
              return cell
          },
          configureSupplementaryView: { dataSource, collectionView, kind, indexPath in
              let header = collectionView.dequeueReusableSupplementaryView(
                ofKind: kind,
                withReuseIdentifier: "Header",
                for: indexPath
              ) as! HourlyWeatherSectionHeader
              header.summary.text = dataSource.sectionModels[indexPath.section].header
              return header
          }
        )

        collectionView.delegate = self

        bindViewModel()
    }

    private func bindViewModel() {
        let trigger = Driver.just(())
        let input = HourlyWeatherViewModel.Input(trigger: trigger)

        let output = viewModel.transform(input: input)

        output.sections
          .drive(collectionView.rx.items(dataSource: dataSource))
          .disposed(by: bag)
    }

    private func formatDate(_ date: Date) -> String? {
       let formatter = DateFormatter()
        formatter.dateStyle = .none
        formatter.timeStyle = .short
        return formatter.string(from: date)
    }

    private func formatTemperature(_ temperature: Measurement<UnitTemperature>?) -> String? {
        guard let temperature = temperature else { return nil }
        let formatter = MeasurementFormatter()
        formatter.unitOptions = .temperatureWithoutUnit
        let numFormatter = NumberFormatter()
        numFormatter.numberStyle = .decimal
        numFormatter.maximumFractionDigits = 0
        formatter.numberFormatter = numFormatter
        return formatter.string(from: temperature)
    }

    private func formatProbablity(_ probability: Double?) -> String? {
        guard let probability = probability else { return nil }
        let formatter = NumberFormatter()
        formatter.numberStyle = .percent
        formatter.maximumFractionDigits = 0
        return formatter.string(for: probability)
    }

    private func iconString(with icon: Icon) -> String {
        switch icon {
        case .clearDay: return Icons.clearDay
        case .clearNight: return Icons.clearNight
        case .rain: return Icons.rain
        case .snow: return Icons.snow
        case .sleet: return Icons.sleet
        case .wind: return Icons.wind
        case .fog: return Icons.fog
        case .cloudy: return Icons.fog
        case .partlyCloudyDay: return Icons.partlyCloudyDay
        case .partlyCloudyNight: return Icons.partlyCloudyNight
        }
    }
}

extension HourlyWeatherViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(
      _ collectionView: UICollectionView,
      layout: UICollectionViewLayout,
      sizeForItemAt indexPath: IndexPath
    ) -> CGSize {
        return CGSize(
          width: collectionView.bounds.width,
          height: collectionView.bounds.height / 10
        )
    }

    func collectionView(
      _ collectionView: UICollectionView,
      layout: UICollectionViewLayout,
      referenceSizeForHeaderInSection section: Int
    ) -> CGSize {
        return CGSize(
          width: collectionView.bounds.width,
          height: collectionView.bounds.height * 0.20
        )
    }
}

import Foundation
import RxSwift
import RxCocoa

final class HourlyWeatherViewModel {

    private let forecastService: ForecastServicing
    private let locationService: LocationServicing

    init(
      forecastService: ForecastServicing,
      locationService: LocationServicing
    ) {
        self.forecastService = forecastService
        self.locationService = locationService
    }

    struct Input {
        let trigger: Driver<Void>
    }

    struct Output {
        let sections: Driver<[HourlyWeatherSection]>
    }

    func transform(input: Input) -> Output {
        let location = locationService.locations
          .map { $0.last! }
          .debug("locations")
          .share(replay: 1)

        let forecasts = input.trigger
          .asObservable()
          .flatMap { _ in location }
          .flatMapLatest { self.forecastService.hourlyWeather($0) }
          .share(replay: 1)

        let sections = forecasts
          .map { forecasts -> [HourlyWeatherSection] in
              let section = HourlyWeatherSection(
                header: forecasts.summary,
                items: forecasts.hours
              )
              return [section]
          }
          .debug("Sections")
          .asDriver(onErrorJustReturn: [])

        return Output(sections: sections)

    }

}

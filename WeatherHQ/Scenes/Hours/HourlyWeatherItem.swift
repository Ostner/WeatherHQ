import UIKit

final class HourlyWeatherCell: UICollectionViewCell {

    @IBOutlet weak var hour: UILabel!
    @IBOutlet weak var icon: UILabel!
    @IBOutlet weak var temperature: UILabel!
    @IBOutlet weak var temperatureIcon: UILabel!
    @IBOutlet weak var rainPropability: UILabel!
    @IBOutlet weak var rainPropabilityIcon: UILabel!
}

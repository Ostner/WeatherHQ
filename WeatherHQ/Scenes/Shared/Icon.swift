import Foundation

final class Icons {

    static var clearDay: String { return "\u{f00d}" }

    static var clearNight: String { return "\u{f02e}" }

    static var rain: String { return "\u{f019}" }

    static var snow: String { return "\u{f01b}" }

    static var sleet: String { return "\u{f0b5}"}

    static var wind: String { return "\u{f050}" }

    static var fog: String { return "\u{f014}" }

    static var cloudy: String { return "\u{f013}" }

    static var partlyCloudyDay: String { return "\u{f002}" }

    static var partlyCloudyNight: String { return "\u{f086}" }
}

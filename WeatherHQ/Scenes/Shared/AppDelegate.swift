import UIKit
import Moya
import CoreLocation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(
      _ application: UIApplication,
      didFinishLaunchingWithOptions opts: [UIApplication.LaunchOptionsKey: Any]?
    ) -> Bool {
        window = UIWindow()

        let forecastProvider = MoyaProvider<DarkSkyAPI>(
          plugins: [NetworkLoggerPlugin(verbose: false)]
        )
        let locationManager = CLLocationManager()
        locationManager.distanceFilter = 200
        let locationService = LocationService(locationProvider: locationManager)
        let forecastService = ForecastService(provider: forecastProvider)

        let currentWeatherVC = CurrentWeatherViewController.makeFromStoryboard()
        let currentWeatherVM = CurrentWeatherViewModel(
          forecastService: forecastService,
          locationService: locationService
        )
        currentWeatherVC.viewModel = currentWeatherVM

        let hourlyWeatherVC = HourlyWeatherViewController.makeFromStoryboard()
        let hourlyWeatherVM = HourlyWeatherViewModel(
          forecastService: forecastService,
          locationService: locationService
        )
        hourlyWeatherVC.viewModel = hourlyWeatherVM

        let tbc = UITabBarController()
        let vcs = [currentWeatherVC, hourlyWeatherVC]
        tbc.setViewControllers(vcs, animated: false)

        window?.rootViewController = tbc
        window?.makeKeyAndVisible()

        return true
    }

}

